# CI-tron executor

The CI-tron executor is the service that coordinates different services to
enable time-sharing test machines, AKA devices under test (DUTs).

This service can be interacted with using
[executorctl](https://gitlab.freedesktop.org/gfx-ci/ci-tron/-/blob/master/executor/client/),
our client, and/or our
[REST API](https://gfx-ci.pages.freedesktop.org/ci-tron/docs/executor.html#id1).

Take a look to our full documentation at
[https://gfx-ci.pages.freedesktop.org/ci-tron/](https://gfx-ci.pages.freedesktop.org/ci-tron/)
