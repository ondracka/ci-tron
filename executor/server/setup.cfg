[metadata]
name = valve_gfx_ci.executor.server
version = 0.0.3
author = Martin Roukala
author_email = martin.roukala@mupuf.org
description = Valve GFX CI's executor
long_description = file: README.md
long_description_content_type = text/markdown
url = https://gitlab.freedesktop.org/gfx-ci/ci-tron/-/tree/master/executor/server
project_urls =
    Bug Tracker = https://gitlab.freedesktop.org/gfx-ci/ci-tron/issues
classifiers =
    Programming Language :: Python :: 3
    License :: OSI Approved :: MIT License
    Operating System :: OS Independent

[options]
package_dir =
  = src
install_requires =
    backports.cached-property;python_version<'3.8'
    requests>=2,<3
    easysnmp==0.2.5
    Flask>=2.2,<3
    marshmallow>=3.12,<3.13
    pydantic>=1,<2
    python-dateutil>=2.8,<3
    PyYAML>=6,<7
    minio>=7.0,<7.1
    inotify-simple==1.3.5
    Jinja2==3.0.3
    deepdiff==5.7.0
    psutil>=5.9,<6
    waitress>=2.1.2,<3
    # NOTE: The upstream version was broken with the update to Urllib3 2.0
    # Let's use our fork until requests_unixsocket pulls our fixes:
    # https://github.com/msabramo/requests-unixsocket/pull/69
    requests_unixsocket@https://github.com/mupuf/requests-unixsocket/releases/download/v0.2.1/requests-unixsocket-0.2.1.dev122.tar.gz
    # NOTE: We use the version 0.12.12 from directly from GitHub because it
    # contains a switch from the deprecated lockfile to filelock, but is is not
    # available anymore on PyPI due to it being a breaking change in a patch
    # update.
    # See https://github.com/ionrock/cachecontrol/issues/286 for more information
    # TODO: Let's switch to the PyPI version when v0.13 gets released.
    CacheControl@https://github.com/ionrock/cachecontrol/archive/refs/tags/v0.12.12.tar.gz
    # FIXME: This works around an incompatibility between CacheControl and urllib3 v3:
    # URL: https://github.com/ionrock/cachecontrol/issues/293
    urllib3<2

tests_requires =
    freezegun==1.1.0
    responses
    coverage
    pytest
include_package_data = True

# TODO: upgrade Flask, PyYAML, minio, marshmallow

packages = find_namespace:
python_requires = >=3.6

[options.packages.find]
where = src

[options.entry_points]
console_scripts =
    executor = valve_gfx_ci.executor.server:run

[pycodestyle]
max-line-length = 160

# Tox configuration
[tox:tox]
envlist = pep8,py3-coverage
skipsdist = True

[testenv:pep8]
deps = flake8
commands=flake8 src/

[flake8]
exclude = .tox, .git, __pycache__, .venv
max-line-length = 120

[testenv:py3-coverage]
sitepackages = true
basepython = python3
setenv =
    COVERAGE_PROCESS_START=./.coveragerc
allowlist_externals = coverage
deps =
    {[options]install_requires}
    {[options]tests_requires}
commands =
    coverage erase
    coverage run --parallel-mode --source=src/valve_gfx_ci/executor/server/ -m pytest -o "testpaths=src/valve_gfx_ci/executor/server/tests"
    coverage combine
    coverage html
    coverage report --fail-under 100 -m --omit="src/valve_gfx_ci/executor/server/__init__.py,src/valve_gfx_ci/executor/server/app.py,src/valve_gfx_ci/executor/server/executor.py,src/valve_gfx_ci/executor/server/mars.py,src/valve_gfx_ci/executor/server/dut.py,src/valve_gfx_ci/executor/server/tftpd.py"
    coverage report
