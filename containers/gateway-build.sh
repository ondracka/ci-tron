#!/bin/bash

set -ex

. containers/build_functions.sh
. containers/gateway_functions.sh

build() {
    buildcntr=$(buildah from -v `pwd`:/app/ci-tron --dns=none --isolation=chroot $EXTRA_BUILDAH_FROM_ARGS $BASE_IMAGE)
    buildmnt=$(buildah mount $buildcntr)

    buildah config --workingdir /app/ci-tron/ansible $buildcntr

    # The Gitlab runner cache deliberately chmod 777's all
    # directories. This upsets ansible and there's nothing we can
    # really do about it in our repo. See
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4187
    $buildah_run $buildcntr chmod -R o-w /app/ci-tron/ansible
    $buildah_run $buildcntr ansible-lint --version
    $buildah_run $buildcntr ansible-lint -x yaml[line-length] -x yaml[braces] -x yaml[commas] -x name[casing] --strict
    $buildah_run $buildcntr ansible-playbook --syntax-check gateway.yml
    $buildah_run $buildcntr ansible-playbook $ANSIBLE_EXTRA_ARGS ./gateway.yml -l localhost

    # Remove useless files
    ci-tron_cleanup

    # ansible should not be modifying some directories in the container and
    # there's no good way to enforce that while the playbook is running, so
    # this throws an error if the "do not touch" paths exist... which implies
    # that ansible created them and probably modified their contents.
    # See: https://gitlab.freedesktop.org/gfx-ci/ci-tron/-/issues/87
    for dir in "/config" "/cache"; do
        if $buildah_run $buildcntr bash -c "[ -a $dir ]" 2>/dev/null; then
            echo "ERROR: directories/files should not exist, which probably means ansible created them:"
            $buildah_run $buildcntr find "$dir"
            exit 1
        fi
    done

    buildah config --entrypoint /sbin/init $buildcntr
}

build_and_push_container
