#!/bin/bash
#
# Run the given command + options in a new network namespace connected to the
# host's default route.
# slirp4netns's options and usage below was largely inspired by the tests for
# it here: https://github.com/rootless-containers/slirp4netns/tree/master/tests
#
set -euo pipefail

# note: this dir needs to be static since this script is invoked again by
# itself, so if the dir is dynamic at runtime (e.g. mktemp), the next
# invocation won't be able to find any of the opened files required for
# signaling slirp4netns
tmpdir=/tmp/run_net
mkdir -p "$tmpdir"

# signals to the script that it should run the payload exe when the new network
# namespace is up
iamachild="${IAMACHILD:-false}"

run_exe() {
	timeout_sec=30
	network_up=false
	started=$(date +%s)

	while [ $(( $(date +%s) - started )) -lt $timeout_sec ]; do
		# poll the ready file for status
		if grep -q 1 "$tmpdir/net_ready" 2>/dev/null; then
			network_up=true
			break
		fi
		sleep 1
	done

	if ! $network_up; then
		echo "slirp4netns failed to configure network"
		exit 1
	fi

	cleanup() {
		rm -f /tmp/run_net/net_remove
		exit "$ret"
	}
	trap "cleanup" EXIT

	# we want to capture the return code in case it's non-zero, so don't exit
	# immediately
	set +e
	"$@"
	ret=$?
	exit $ret
}

setup_net() {
	touch "$tmpdir/net_remove"
	touch "$tmpdir/net_ready"

	# re-run this script with unshare, for actually running the given
	# exe/payload
	IAMACHILD=true unshare --user --map-root-user --net --mount "$0" -- "$@" &
	childpid=$!

	# give some time for unshare to start the script (else slirp4netns may lose
	# the race and fail)
	sleep 1

	# fd 10 is used to signal when the network should be torn down
	exec 10< <(while test -e /tmp/run_net/net_remove; do sleep 0.1; done)

	# note: ip changed from default since it may conflict with what podman ends
	# up with (podman uses slirp4netns too), which could lead to multiple
	# (v)NICs having the same IP when this is run under podman...
	slirp4netns --disable-host-loopback --configure \
		--cidr=10.10.10.0/24 \
		--exit-fd 10 --ready-fd 11 \
		"$childpid" tap0 \
		11>"$tmpdir/net_ready"
	slirppid=$!

	# set later when waiting for the child to quit
	ret=0

	cleanup() {
		rm -rf "$tmpdir"
		kill -9 "$childpid" 2>/dev/null || true
		kill -9 "$slirppid" 2>/dev/null || true
		exit "$ret"
	}
	trap cleanup EXIT

	# wait returns the child's return code, which might be non-zero. we want to
	# capture it
	set +e

	wait -f "$childpid"
	ret=$?
	exit $ret
}

if $iamachild; then
	# strip off the --
	shift
	run_exe "$@"
else
	# TODO: improve this....
	rm -rf "$tmpdir"
	mkdir -p "$tmpdir"

	setup_net "$@"
fi
