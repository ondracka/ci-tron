#!/bin/bash

set -ex

. containers/build_functions.sh
. containers/gateway_functions.sh

build() {
	buildcntr=$(buildah from -v `pwd`:/app/ci-tron --dns=none --isolation=chroot $EXTRA_BUILDAH_FROM_ARGS $BASE_IMAGE)
	buildmnt=$(buildah mount $buildcntr)

	# Remove left-over files from fedora's build process
	$buildah_run $buildcntr sh -c 'rm /root/*'

	cat <<EOF >$buildmnt/etc/resolv.conf
nameserver 1.1.1.1
nameserver 8.8.8.8
nameserver 4.4.4.4
EOF

	# Switch to dnf5
	$buildah_run $buildcntr dnf --refresh -y install dnf5 python3-libdnf5
	$buildah_run $buildcntr dnf -y clean all
	$buildah_run $buildcntr rpm -e dnf yum python3-dnf
	$buildah_run $buildcntr dnf5 -y autoremove
	$buildah_run $buildcntr ln -s /usr/bin/dnf5 /usr/bin/dnf

	$buildah_run $buildcntr dnf5 -y --refresh upgrade
	$buildah_run $buildcntr dnf5 -y --setopt install_weak_deps=False install git-core ansible-core ansible-lint procps-ng

	# Run ansible playbook, but *only* to install SW. Configuration is done in
	# gateway-build.sh
	buildah config --workingdir /app/ci-tron/ansible $buildcntr
	# The Gitlab runner cache deliberately chmod 777's all
	# directories. This upsets ansible and there's nothing we can
	# really do about it in our repo. See
	# https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4187
	$buildah_run $buildcntr chmod -R o-w /app/ci-tron/ansible
	$buildah_run $buildcntr ansible-galaxy collection install -r ./requirements.yml
	# 'install' tag should *only* install SW
	$buildah_run $buildcntr ansible-playbook $ANSIBLE_EXTRA_ARGS ./gateway.yml -l localhost --tags install

	# Remove useless files
	ci-tron_cleanup
}

build_and_push_container
