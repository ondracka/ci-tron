Welcome to ci-tron documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents

   docs/introduction
   docs/installation_and_development
   docs/executor
